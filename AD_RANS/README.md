# Test case description

This test case consists of a 3D box grid, used to calculate wind 
turbine wakes in atmospheric turbulence with RANS. The DTU-10MW 
reference wind turbine is used in all cases. The turbulence is 
modeled with the k-epsilon-f_P model. More information can be 
found in the Ph.D. thesis of Paul van der Laan.

## Grid

The grid consists of 192 x 48 x 96 cells in the stream-wise (x),
span-wise (y) and normal (z) directions, respectively. The domain
is 50D long, 16D m wide and 16D tall, where D represents a rotor 
diameter of 178 m. Inside the flow domain, a wake domain is defined 
with dimensions 19 x 3 x 2 D^3, where a maximum spacing of D/8 is 
specified. A logarithmic profile is set at the inlet (x = 0 and 
z = 16 D) and a outlet is placed at x = 50 D. The side boundaries 
are symmetric and the bottom boundary is a rough wall.

The AD grid is made with generate_AD_gridfile.py, found in:
ellipsys3d/tools/ADgrid/

## Case 1: single_cstF_full

A single wind turbine with constant fully distributed forces.

## Case 2: single_cstF_scaling

A single wind turbine with constant forces, where a normalized AD 
grid is scaled with a given Ct and Cp.

## Case 3: single_cstF_Jou

A single wind turbine with constant (total) forces, where the
force distributions are based on a Joukowsky rotor (constant
circulation). The normal and tangential forces are effected
by the inflow and are rescaled to with the provided Ct and Cp.
For more info see: https://doi.org/10.1016/j.renene.2019.09.134

## Case 4: single_varF_scaling

A single wind turbine with variable forces, where a normalized AD 
grid is scaled with a given Ct* and Cp*. Ct* and Cp* are the thrust 
and power coefficients based on the averaged AD velocity U_AD. The 
Ct*(U_AD) and Cp*(U_AD) curves are determined from precursor 
simulations based on Case 2, using different free-stream velocities.
More information on this method is found in: DOI: 10.1002/we.1816.

## Case 5: single_varF_airfoil

A single wind turbine with variable forces, based on a BEM method
using airfoil data. The tip correction of Pirrung et al. (2019) 
is used.

## Case 6: double_varF_scaling

Two alined wind turbines with 5D spacing using variable forces, 
as explained in Case 2.

## Case 7: double_varF_airfoil

Two alined wind turbines with 5D spacing using variable forces,  
as explained in Case 5. The tip correction of Shen is used.
