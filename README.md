# EllipSys3D Actuator Shape Test Cases

This repository contains a series of test cases related to simulations
of geometries represented by Actuator Shapes for regression testing of
the code.
