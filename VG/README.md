# Test case description

This test case consists of VGs using the BAY model.

## Grid

The grid is an extruded O-grid and has 256 x 128 x 32
cells in the chordwise, normal and spanwise
directions, respectively, with a domain radius of 15c. Symmetry conditions are applied on the side boundaries.

## Case 1: FFA_W3_308_pseudo3d

Four VGs are placed on an extruded FFA airfoil, representing an infinite counter rotating VG array, placed at 20% chord. The VGs have a height of 1% chord.
